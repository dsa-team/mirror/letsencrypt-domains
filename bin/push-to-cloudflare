#!/usr/bin/python3

import os
import sys

import requests


BASE_URL='https://api.cloudflare.com/client/v4'
RES = "/srv/letsencrypt.debian.org/var/result"


class CFAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = 'Bearer %s' % self.token
        return r


def post_cert(session, zone_id, domain):
    key = os.path.join(RES, domain + '.key')
    with open(key, 'r', encoding='ascii') as f:
        ssl_key = f.read()
    cert = os.path.join(RES, domain + '.crt')
    with open(cert, 'r', encoding='ascii') as f:
        ssl_crt = f.read()
    r = session.get(BASE_URL + '/zones/%s/custom_certificates' % zone_id)
    r.raise_for_status()
    known_certs = r.json()['result']
    prev_id = None
    for cert in known_certs:
        if domain in cert['hosts']:
            prev_id = cert['id']
            break
    if prev_id is None:
        r = session.post(BASE_URL + '/zones/%s/custom_certificates' % zone_id,
                json={'certificate': ssl_crt, 'private_key': ssl_key, "type":"sni_custom"})
    else:
        r = session.patch(BASE_URL + '/zones/%s/custom_certificates/%s' % (zone_id, prev_id),
                json={'certificate': ssl_crt, 'private_key': ssl_key})
    r.raise_for_status()
    return r.json()['result']['id']


def main():
    if len(sys.argv) != 2:
        raise SystemExit("usage: %s domain" % sys.argv[0])
    domain = sys.argv[1]
    api_token = os.environ['CLOUDFLARE_TOKEN']
    zone_id = os.environ['CLOUDFLARE_ZONE']
    session = requests.Session()
    session.auth = CFAuth(api_token)
    post_cert(session, zone_id, domain)

if __name__ == '__main__':
    main()

